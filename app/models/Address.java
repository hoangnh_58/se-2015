package models;

import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import models.Product;
import javax.persistence.*;
import javax.persistence.Id;
import play.db.ebean.Model;
import javax.persistence.Entity;

@Entity
public class Address extends Model
{
    @Id
    public Long id;

    @OneToOne (mappedBy = "address")
    public Warehouse warehouse;

    public String street;
    public String number;
    public String postalCode;
    public String city;
    public String country;
}