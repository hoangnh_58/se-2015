package models;

import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import models.Product;
import javax.persistence.*;
import javax.persistence.Id;
import play.db.ebean.Model;

@Entity
public class Warehouse extends Model
{
    @Id
    public Long id;

    public String name;

    @OneToOne
    public Address address;

    @OneToMany (mappedBy = "warehouse")
    public List<StockItem> stock = new ArrayList();  // trường quan hệ

    public String toString() {
        return name;
    }

    public static Finder<Long, Warehouse> find =
            new Finder<>(Long.class, Warehouse.class);
    public static Warehouse findById(Long id) {
        return find.byId(id);
    }
}