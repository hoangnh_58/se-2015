package models;

import com.avaje.ebean.Page;
import play.mvc.PathBindable;
import java.util.Date;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;

@Entity
public class Product extends Model implements PathBindable<Product>
{
    @Id
    public Long id;
    @Constraints.Required
    public String ean;
    @Constraints.Required
    public String name;
    public String description;
    public Date date;
    @OneToMany(mappedBy="product")
    public List<StockItem> stockItems;

    public byte[] picture;
    //public List<Tag> tags;    // trường quan hệ nối với Tag
    @ManyToMany
    public List<Tag> tags = new LinkedList<Tag>();

    public Product() {}
    public Product(String ean, String name, String description) {
        this.ean = ean;
        this.name = name;
        this.description = description;
    }
    public String toString() {
        return String.format("%s - %s", ean, name);
    }

    //khai báo một đối tượng finder trong lớp Product để truy vấn các thực thể Product trong DB.
    public static Finder<Long,Product> find = new Finder<Long,Product>(Long.class, Product.class);

    public static List<Product> findAll()
    {
        return find.all();
    }

    public void delete() {
        		for (Tag tag : tags) {
            			tag.products.remove(this);
            			tag.save();
            		}
        		super.delete();
        	}

    public static Page<Product> find(int page) {  // Trả về trang thay vì List
        return find.where()
                .orderBy("id asc")     // Sắp xếp tăng dần theo id
                .findPagingList(10)    // Quy định kích thước của trang
                .setFetchAhead(false)  // Có cần lấy tất cả dữ liệu một thể?
                .getPage(page);    // Lấy trang hiện tại, bắt đầu từ trang 0
    }

    public static Product findByEan(String ean)
    {
        return find.where().eq("ean", ean).findUnique();
    }


    public static List<Product> findByName(String term) {
        final List<Product> results = new ArrayList<Product>();
        for (Product candidate : products) {
            if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
                results.add(candidate);
            }
        }
        return results;
    }

    public static boolean remove(Product product) {
        return products.remove(product);
    }
/*
    public void save() {
        products.remove(findByEan(this.ean));
        products.add(this);
    }
*/

    private static List<Product> products;

    @Override
    public Product bind(String key, String value) {
        return findByEan(value);
    }

    @Override
    public String unbind(String key) {
        return ean;
    }

    @Override
    public String javascriptUnbind() {
        return ean;
    }

}