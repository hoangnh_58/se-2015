package controllers;


import java.util.ArrayList;
import java.util.List;
import play.mvc.*;
import models.StockItem;

public class StockItems extends Controller
{
    public static Result index()
    {
        List<StockItem> items = StockItem.find
                .where()
                .ge("quantity", 300)
                .orderBy("quantity desc")
                .setMaxRows(5)
                .findList();
        return ok(items.toString());
    }
}